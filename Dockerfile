# Docker image for running PHP applications on the App Engine Flexible Runtime 
FROM gcr.io/google-appengine/php

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Install Imagick and Imagick(PHP extension)
RUN apt-get update -y && apt-get install imagemagick php-imagick -y 

# Install nano text editor
RUN apt-get install nano -y

# Append to php.ini file
RUN echo "extension=imagick.so" >> /opt/php72/lib/php.ini