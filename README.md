Running locally (i.e. using Docker Desktop for Windows)

1. Build the image with: docker build --tag=customphp .
2. Run the image with: docker run -p 4000:8080 customphp
3. Go to https://20190716t224754-dot-frontend-php-20190614.appspot.com and Upload a file.
	Note: this is an older version of the frontend that doesn't auto-trigger the backend
4. Manually trigger the backend's index.php (which triggers container_script.php) by accessing
		local container over HTTP using http://localhost:4000
5. Go to https://20190716t224754-dot-frontend-php-20190614.appspot.com/download.php and click Check button. 
		Repeat if "no messages to process" appears.
6. Click link to open modified image in new tab/window.


Deploy to App Engine Flex environment

Setup

    Install the Google Cloud SDK (https://developers.google.com/cloud/sdk/).

Deploy

	Deploy with gcloud

	Open a command prompt, navigate to the folder with the code, and do the following: 
		# YOUR_PROJECT_ID should be a DIFFERENT project than the frontend uses; must contain
		# an App Engine Flex app
		gcloud config set project YOUR_PROJECT_ID
		gcloud app deploy


Test
1. Go to https://frontend-php-20190614.appspot.com and upload a file.
2. Use 'download' link or go to https://frontend-php-20190614.appspot.com/download.php and click Check button. 
		Repeat if "no messages to process" appears.
3. Click link to open modified image in new tab/window.
