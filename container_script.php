<?php

/**
 * Title: container_script.php - "Backend" container script for Image Resize/Compression as a Service application
 *
 * Date: June-July 2019
 * Authors: Mike MacDonald, with input from Ian Samuel
 * Code attributions: Google Cloud Client Libraries for PHP, PHP: ImageMagick - Manual
 * 	(https://www.php.net/manual/en/book.imagick.php) and other code attributions as
 *	indicated in comments.
 * Developed for PHP 7.2 or later
 *
 * This script checks Google Cloud Pub/Sub for messages submitted by the frontend client interface
 * and acts on EACH message in the following way:
 *
 * 		1. Assigns elements of the message (data and attributes) to variables for use in this script
 *
 * 		2. Makes a copy of the Cloud Storage object identified in the pub/sub message
 *			and names it by pre-pending the sessionId and requested action to the old
 *			name using the following format or convention: sessionId/action-oldname
 *			(e.g. 950f2a8eb21540085016e16823a0f52e/resize_thumbnail-IMG_20190319_232737164.jpg).
 *
 * 		3. Modifies the access permissions of the new object to make it public.
 *
 * 		4. Selects one of four different cases that correspond to each of the possible/expected actions
 *			being requested on the object.
 *
 * 		5. Executes the function(s) associated with each case; each function downloads the object to temporary
 * 			local storage before acting on the object.
 *
 * 		6. Uploads the modified object to Cloud Storage, overwriting the copy created earlier.
 *
 * 		7. Writes a message to Cloud Pub/Sub indicating the action is complete plus attributes about the
 * 			modified object and the sessionId that was used in naming the modified object.
 *
 * If there are no messages in the pub/sub subscription, an exception is caught and a message is
 * 	echoed to standard output.
 *
 * All information echoed or printed by this script appears in the web browser when this script
 * 	is executed via an HTTP call from another script (e.g. frontend) or directly via client's browser.
 *
 */

//Includes the autoloader for libraries installed with composer
require 'vendor/autoload.php';

//Imports the Google Cloud client library
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\PubSub\PubSubClient;

//Global variables
$projectId = 'frontend-php-20190614'; //Your Google Cloud Platform project ID
$bucketName = "frontend-php-20190614.appspot.com"; // storage bucket name
$messages_array = array();

/**
 * Receive messages from Pub/Sub.
 *
 * @param string $projectId
 * @param string $subscriptionName
 *
 * @return void
 * See: https://cloud.google.com/pubsub/docs/quickstart-client-libraries#pubsub-client-libraries-php
*/
function pull_messages($projectId, $subscriptionName){
    $i = 0;
	$pubsub = new PubSubClient([
        'projectId' => $projectId,
		//Can comment out the next line if deployed; required to run locally
		'keyFilePath' => 'frontend-php-20190614-803c3809bab9.json'
    ]);
    $subscription = $pubsub->subscription($subscriptionName);
    if ($subscription->exists()){

		foreach ($subscription->pull() as $message) {
			printf('Message (data and attributes): %s' . ' ', $message->data() . PHP_EOL);
			//printf(' %s' . PHP_EOL, $message->attributes());
			$attributes = $message->attributes();
			//var_dump($attributes); // testing by dumping the entire array
			//echo '  name: ' . $attributes['name'] . PHP_EOL;
			//echo '  bucket: ' . $attributes['bucket'] . PHP_EOL;
			//echo '  storageClass: ' . $attributes['storageClass'] . PHP_EOL;
			//echo '  id: ' . $attributes['id'] . PHP_EOL;
			//echo '  size: ' . $attributes['size'] . PHP_EOL;
			//echo '  updated: ' . $attributes['updated'] . PHP_EOL;
			//echo '  sessionId: ' . $attributes['sessionId'] . PHP_EOL;

			// Acknowledge the Pub/Sub message has been received, so it will not be pulled multiple times.
			$subscription->acknowledge($message);

			$messages_array[$i] = $message;
			$i++;
		}
		if(isset($messages_array)){
			return $messages_array;
		} else throw new Exception('No messages to process.');

	} else throw new Exception('Subscription does not exist.');
}

/**
 * Download an object from Cloud Storage and save it as a local file.
 *
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of your Google Cloud object.
 * @param string $destination the local destination to save the encrypted object.
 *
 * @return void
 * See: https://cloud.google.com/storage/docs/downloading-objects#storage-download-object-php
 */
function download_object($projectId, $bucketName, $objectName, $destination){
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			'keyFilePath' => 'frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

    $bucket = $storage->bucket($bucketName);
    $object = $bucket->object($objectName);

    $object->downloadToFile($destination);

	printf('Downloaded %s to %s' . PHP_EOL, $object->gcsUri(), basename($destination));
}

/**
 * Upload an object to Cloud Storage.
 *
 * @param string $projectId  The Google project ID.
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $filename the name of your Google Cloud object.
 * @param string $dir The "folder" to upload the object to (i.e. will be appended to the object's name)
 *
 * @return void
 * See: https://cloud.google.com/storage/docs/uploading-objects
  * and https://cloud.google.com/storage/docs/downloading-objects#storage-download-object-php
 */
function upload_object($projectId, $bucketName, $filename, $dir){
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			'keyFilePath' => 'frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

	//$storage = new StorageClient();
	$bucket = $storage->bucket($bucketName);

	$basename = basename($filename);

	//Upload a file to the bucket
	$options = [
				'name' => "${dir}/${basename}"
				];

	$object = $bucket->upload(
				fopen($filename, 'r'),
				$options
			);

	printf('Uploaded %s to %s' . PHP_EOL, $filename, $object->gcsUri());

	return $object;

}

/**
 * Make an object publicly accessible.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 * @param string $objectName the name of your Cloud Storage object.
 * See:
 *	https://cloud.google.com/storage/docs/access-control/making-data-public#storage-make-object-public-php
 *
 * @return void
 */
function make_public($projectId, $bucketName, $objectName){

    $config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			'keyFilePath' => 'frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

	$bucket = $storage->bucket($bucketName);

	$object = $bucket->object($objectName);

	$object->update(['acl' => []], ['predefinedAcl' => 'PUBLICREAD']);

	printf('gs://%s/%s is now public' . PHP_EOL, $bucketName, $objectName);
}

/**
 * Make a copy of an object in Cloud Storage.
 *
 * @param string $projectId  The Google project ID.
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of your Google Cloud object.
 * @param string $copiedObjectName the new name of your object.
 *
 * @return object $copiedObject the copied object.
 * See:
 * 	https://googleapis.github.io/google-cloud-php/#/docs/google-cloud/v0.105.0/storage/storageobject?method=copy
 */
function copy_object($projectId, $bucketName, $objectName, $copiedObjectName){
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			'keyFilePath' => 'frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

	//$storage = new StorageClient();
	$bucket = $storage->bucket($bucketName);

	$object = $bucket->object($objectName);

	//Copy a file to the bucket
	$copiedObject = $object->copy($bucket, [
			'name' => $copiedObjectName
		]);

	printf('Copied %s to %s' . PHP_EOL, $objectName, $copiedObject->gcsUri());

	return $copiedObject;

}

/**
 * Create a thumbnail of an image using Imagick.
 *
 * @param string $source  path to source file
 * @param string $target path to target file
 *
 * @return void
 * See:
 */
function create_thumbnail($source, $target){

	$image = new Imagick($source);


	// If 0 is provided as a width or height parameter,
	// aspect ratio is maintained
	$image->thumbnailImage(100, 0);

	$image->writeImage($target); // /tmp is writeable, but not persistent across instances

	printf ('Made a thumbnail.' . PHP_EOL);

}
/**
 * Resize an image using Imagick.
 *
 * @param string $source  path to source file
 * @param float $scale percentage of original size in decimal
 * @param string $target path to target file
 *
* @return void
 * See:
 */
function resize_image($source, $scale, $target){

	$image = new Imagick($source);

	// If 0 is provided as a width or height parameter,
	// aspect ratio is maintained
	$image->thumbnailImage($image->getImageWidth()*$scale, 0);

	$image->writeImage($target); // /tmp is writable, but not persistent across instances

	printf ('Resized by %s.' . PHP_EOL, $scale);

}

/**
 * Compress an image using Imagick.
 *
 * @param string $source  path to source file
 * @param int $quality image compression quality as percentage of original
 * @param string $target path to target file
 *
* @return void
 * See:
 */
function compress_image($source, $quality, $target){

	$image = new Imagick($source);

	$image->gaussianBlurImage(0.8, 10);
	$image->setImageCompressionQuality($quality);

	$image->writeImage($target); // /tmp is writeable, but not persistent across instances

	printf ('Compressed to %s percent of original quality.' . PHP_EOL, $quality);

	$im = new Imagick($source);
	$src_size = $im->getImageLength();
	//echo $src_size . PHP_EOL;
	$im = new Imagick($target);
	$target_size = $im->getImageLength();
	//echo $target_size . PHP_EOL;
	if($target_size > $src_size){
		printf('No filesize reduction gained by compressing. Try resizing image instead.' . PHP_EOL);
	}

}

/**
 * Publishes a message for a Pub/Sub topic.
 *
 * @param string $projectId  The Google project ID.
 * @param string $topicName  The Pub/Sub topic name.
 * @param string $message  The message to publish.
 *
 * See: https://cloud.google.com/pubsub/docs/quickstart-client-libraries#pubsub-client-libraries-php
  * or https://cloud.google.com/pubsub/docs/publisher
 */
function publish_message($projectId, $topicName, $message, $attributes){
    $pubsub = new PubSubClient([
        'projectId' => $projectId,
		//Can comment out the next line if deployed; required for running locally
		'keyFilePath' => 'frontend-php-20190614-803c3809bab9.json'
    ]);
    $topic = $pubsub->topic($topicName);
    $topic->publish([
        'data' => $message,
        'attributes' => $attributes
    ]);
    print('Message published' . PHP_EOL);
	//print($topic); // testing; should get error that array can't be output at string
}

/**
 *"Main" program
 *
 */

print date("r") . PHP_EOL;

//Pull messages from Cloud Pub/Sub, if any in the subscription
//echo 'Here we are pulling messages from the pub/sub subscription (front-to-back-sub): ' . PHP_EOL;

try {
	$messages_array = pull_messages('frontend-php-20190614', 'front-to-back-sub');

	// Do the work on the image?
	foreach($messages_array as $message){
		$action = $message->data();
		$attributes = $message->attributes();
		$objectName = $attributes['name'];
		$bucketName = $attributes['bucket'];
		$sessionId = $attributes['sessionId'];

		// Use public URL to object in Cloud Storage instead of a "downloaded" file
		$destination = 'http://storage.googleapis.com/' . $bucketName . '/' . $objectName;

		// Make a copy with a new filename; will overwrite this modified image later
		$copiedObject = copy_object($projectId, $bucketName, $objectName, $sessionId . '/' . $action . '-' . $objectName);

		switch($action){
			case 'resize_thumbnail':
				create_thumbnail($destination, "/tmp/${action}-${objectName}"); // function call
				break;
			case 'resize_50':
				$array = explode("_", $action);
				$array[1] = ((float)$array[1]) / 100;
				resize_image($destination, $array[1], "/tmp/${action}-${objectName}"); // function call
				break;
			case 'compress_85':
				$array = explode("_", $action);
				compress_image($destination, $array[1], "/tmp/${action}-${objectName}"); // function call
				break;
			case 'resize_50_compress_85':
				$array = explode("_", $action);
				$array[1] = ((float)$array[1]) / 100;
				// function call
				resize_image($destination, $array[1], "/tmp/${action}-${objectName}");
				// function call
				compress_image('/tmp/' . $action . '-' . $objectName, $array[3], "/tmp/${action}-${objectName}");
				break;
			default:
				echo 'No action to perform.';
				break;
		}

		//Upload a file to the bucket
		// returns object reference to use elsewhere
		$object = upload_object($projectId, $bucketName, "/tmp/${action}-${objectName}", $sessionId);

		//Make the object publicly accessible
		make_public($projectId, $bucketName, "${sessionId}/${action}-${objectName}");

		//Write a message to pub/sub with object metadata
		$info = $object->info();
		$attributes = [
            'name' => $info['name'],
			'bucket' => $info['bucket'],
			'storageClass' => $info['storageClass'],
			'id' => $info['id'],
			'size' => $info['size'],
			'updated' => $info['updated'],
			'sessionId' => $sessionId
        ];
		if (!$object){
			$message = 'Nothing to download.';
		// example: Image ready,gs://frontend-php-20190614.appspot.com/resize_thumbnail-IMG_20190319_232737164.jpg
		} else $message = 'Image ready' . ',' . $object->gcsUri();
		publish_message($projectId, 'back-to-front-topic',$message, $attributes); //function call

	}

} catch (Exception $e){
	echo 'Caught exception: ' . $e->getMessage() . PHP_EOL;
}

?>
