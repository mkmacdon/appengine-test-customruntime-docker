<?php

/**
 * "Front Controller" to handle routing between scripts (required for PHP on App Engine)
 * See: https://stackoverflow.com/questions/54259307/google-cloud-app-engine-app-yaml-php72-issue-with-routing
 * and https://github.com/GoogleCloudPlatform/php-docs-samples/tree/master/appengine/php72
 * or https://github.com/GoogleCloudPlatform/php-docs-samples/blob/master/appengine/php72/front-controller/index.php
 */

 switch (@parse_url($_SERVER['REQUEST_URI'])['path']) {
	case '/':
		require 'container_script.php';
		break;
	case '/container_script.php':
        require 'container_script.php';
        break;
    default:
        break;
}
?>